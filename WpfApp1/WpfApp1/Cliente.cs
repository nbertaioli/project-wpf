﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace WpfApp1
{
    public class Cliente
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Nome")]
        public string Nome { get; set; }

        [JsonProperty("Sobrenome")]
        public string Sobrenome { get; set; }

        [JsonProperty("Telefone")]
        public string Telefone { get; set; }
    }
}

﻿using Newtonsoft.Json;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace WpfApp1
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        const string urlApi = "https://webapplication220200814230438.azurewebsites.net/";

        public MainWindow()
        {
            InitializeComponent();
        }

        // verifica se não existem campos vazios, no caso, todos são obrigatórios.
        private bool ValidarCampos()
        {
            if (txtNome.Text.Trim() != string.Empty || txtSobrenome.Text.Trim() != string.Empty || txtTelefone.Text.Trim() != string.Empty)
                return false;
            else
                return true;
        }

        // chama o método para validar os campos
        // cria um cliente, serializa o transformando em um json
        // chama o método assincrono processarcliente
        // exibe mensagem na tela
        // limpa os campos.
        private async void BtnGerarJson_Click(object sender, RoutedEventArgs e)
        {
            if (!ValidarCampos())
            {
                var Cliente = new Cliente
                {
                    Id = txtId.Text == "Inserido automaticamente" ? Guid.NewGuid().ToString() : txtId.Text,
                    Nome = txtNome.Text,
                    Sobrenome = txtSobrenome.Text,
                    Telefone = txtTelefone.Text
                };

                string Json = JsonConvert.SerializeObject(Cliente, Formatting.Indented);

                await ProcessarCliente(Json);

                MessageBoxResult result = MessageBox.Show("Cliente cadastrado com sucesso.");

                BtnLimparCampos_Click(sender, e);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Favor preencher todos os campos.");
            }
        }

        //realiza a chamada da api para processamento do cliente com as informações obtidas em tela.
        public async Task ProcessarCliente(string Json)
        {
            try
            {
                var procCliente = RestService.For<ICliente>(urlApi);
                await procCliente.PostClienteAsync(Json);

                await CarregarCliente();
            }
            catch (Exception e)
            {
                Console.WriteLine("Falha" + e.Message);
            }
        }

        //realiza a limpeza dos campos da tela e seta o foco no campo nome.
        private void BtnLimparCampos_Click(object sender, RoutedEventArgs e)
        {
            txtId.Text = "Inserido automaticamente";
            txtNome.Text = string.Empty;
            txtSobrenome.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            txtNome.Focus();
        }

        //realiza a chamada da api para carregamento dos clientes cadastrados no banco de dados
        public async Task CarregarCliente()
        {
            try
            {
                var procCliente = RestService.For<ICliente>(urlApi);
                JsonSerializer jsonData = new JsonSerializer();
                var Json = await procCliente.GetClienteAsync();

                List<Cliente> listCliente = new List<Cliente>();

                foreach (var item in Json)
                {
                    listCliente.Add(new Cliente { Id = item.Id, Nome = item.Nome, Sobrenome = item.Sobrenome, Telefone = item.Telefone });
                }
                dataGridCliente.ItemsSource = listCliente;
            }

            catch (Exception e)
            {
                Console.WriteLine("Falha" + e.Message);
            }
        }

        private async void TabItem_Initialized(object sender, EventArgs e)
        {
            await CarregarCliente();
        }

        private async void DeletarCliente(string Json)
        {
            try
            {
                var procCliente = RestService.For<ICliente>(urlApi);
                await procCliente.DeleteClienteAsync(Json);
                await CarregarCliente();
                MessageBoxResult result = MessageBox.Show("Cliente deletado com sucesso.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Falha." + e.Message);
            }
        }

        private void DeleteCliente_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                JsonSerializer jsonString = new JsonSerializer();
                Cliente cliente = (Cliente)dataGridCliente.SelectedItem;
                string Json = JsonConvert.SerializeObject(cliente, Formatting.Indented);

                if (!(string.IsNullOrEmpty(Json)) && !(Json == "null"))
                {
                    DeletarCliente(Json);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Objeto vazio");
            }
        }

        private void AlterarCliente_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cliente cliente = (Cliente)dataGridCliente.SelectedItem;

                TabControlPrincipal.SelectedIndex = 0;

                txtId.Text = cliente.Id;
                txtNome.Text = cliente.Nome;
                txtSobrenome.Text = cliente.Sobrenome;
                txtTelefone.Text = cliente.Telefone;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Objeto vazio.");
            }            
        }
    }
}

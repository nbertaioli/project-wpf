﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WpfApp1
{
    public interface ICliente
    {
        [Get("/api/Cliente")]
        Task<List<Cliente>> GetClienteAsync();

        [Post("/api/Cliente?cliente={Json}")]
        Task<Cliente> PostClienteAsync(string Json);

        [Delete("/api/Cliente?cliente={Json}")]
        Task<Cliente> DeleteClienteAsync(string Json);
    }
}

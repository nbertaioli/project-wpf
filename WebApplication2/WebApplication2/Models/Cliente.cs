﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Cliente
    {
        [JsonProperty("Id")]
        public Guid Id { get; set; }

        [JsonProperty("Nome")]
        public string Nome { get; set; }

        [JsonProperty("Sobrenome")]
        public string Sobrenome { get; set; }

        [JsonProperty("Telefone")]
        public string Telefone { get; set; }

        public Cliente(Guid Id, string Nome, string Sobrenome, string Telefone)
        {
            this.Id = Id;
            this.Nome = Nome;
            this.Sobrenome = Sobrenome;
            this.Telefone = Telefone;
        }
    }
}
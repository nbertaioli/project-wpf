﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Web.Http;
using WebApplication2.App_Start;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class ClienteController : ApiController
    {
        private static List<Cliente> clientes = new List<Cliente>();
        
        SqlCommand sqlCom = new SqlCommand();

        [HttpGet]
        public List<Cliente> GetListClient()
        {
            try
            {
                clientes.Clear();

                Connection con = new Connection();
                sqlCom.Connection = con.Connect();

                sqlCom.CommandText = "SELECT ID, NOME, SOBRENOME, TELEFONE FROM CLIENTES";
                var result = sqlCom.ExecuteReader();

                while (result.Read())
                {
                    Cliente tempClient = new Cliente(Guid.Parse(result["Id"].ToString()), result["Nome"].ToString(), result["Sobrenome"].ToString(), result["Telefone"].ToString());
                    clientes.Add(tempClient);
                }

                con.Disconnect();

                return clientes;
            }
            catch (Exception e)
            {
                Console.WriteLine("Ocorreu um erro ao processar a pesquisa." + e.Message);
            }
            
            return clientes;
        }

        [HttpPost]
        public void PostClient(string @cliente)
        {
            try
            {
                JsonSerializer jsonString = new JsonSerializer();

                Cliente jsonCliente = JsonConvert.DeserializeObject<Cliente>(@cliente); 

                Connection con = new Connection();
                sqlCom.Connection = con.Connect();
                sqlCom.CommandText = "SELECT * FROM Clientes WHERE Id = @id";
                sqlCom.Parameters.AddWithValue("@id", jsonCliente.Id);
                var tempResult = sqlCom.ExecuteReader();

                if (tempResult.HasRows)
                    sqlCom.CommandText = "UPDATE Clientes SET Nome = @nome, Sobrenome = @sobrenome, Telefone = @telefone WHERE Id = @id";
                else
                    sqlCom.CommandText = "INSERT INTO Clientes (Id, Nome, Sobrenome, Telefone) VALUES(@id, @nome, @sobrenome, @telefone)";

                sqlCom.Parameters.AddWithValue("@nome", jsonCliente.Nome);
                sqlCom.Parameters.AddWithValue("@sobrenome", jsonCliente.Sobrenome);
                sqlCom.Parameters.AddWithValue("@telefone", jsonCliente.Telefone);

                con.Disconnect();
                con.Connect();

                var result = sqlCom.ExecuteNonQuery();

                con.Disconnect();
            }
            catch (Exception e)
            {
                Console.WriteLine("Ocorreu um erro ao processar a gravação de registro." + e.Message);
            }
        }

        [HttpDelete]
        public void DeleteClient(string @cliente)
        {
            try
            {
                Connection con = new Connection();
                sqlCom.Connection = con.Connect();

                JsonSerializer jsonString = new JsonSerializer();

                Cliente jsonCliente = JsonConvert.DeserializeObject<Cliente>(@cliente);

                sqlCom.CommandText = "DELETE FROM CLIENTES WHERE ID=@id";
                sqlCom.Parameters.AddWithValue("@id", jsonCliente.Id);
                var result = sqlCom.ExecuteNonQuery();

                con.Disconnect();
            }
            catch (Exception e)
            {
                Console.WriteLine("Ocorreu um erro ao processar a exclusão do registro." + e.Message);
            }
}
    }
}
